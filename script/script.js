/*jslint plusplus: true */

/*---- GLOBAL VARIABLES ----*/
var
MAIN_ARRAY,
DATA_BASE,
ALBUMS_COUNTER,
MAIN_GRID,
MAIN_PREVIEW,
MAIN_LIST,
GRID_RECENTLYADDED,
GRID_CONTAINER,
PREVIEW_CONTAINER,
LIST_CONTAINER,
GRID_UL_SCROLL_POSITION,
PREVIEW_UL_SCROLL_POSITION,
LIST_UL_SCROLL_POSITION,
TOGGLE_BUTTON,
TOGGLE_GRID,
TOGGLE_PREVIEW,
TOGGLE_LIST,
TOGGLE_COUNTER = 0,
CONTENT_GRID_TOGGLE_RECENTLYADDED,
CONTENT_GRID_TOGGLE_ALL;

//main array \w albums data
MAIN_ARRAY = [];

//other variables
DATA_BASE = document.getElementById('data-base');
ALBUMS_COUNTER = document.getElementById('albums-counter');
MAIN_GRID = document.getElementById('main-grid');
MAIN_PREVIEW = document.getElementById('main-preview');
MAIN_LIST = document.getElementById('main-list');
GRID_RECENTLYADDED = document.getElementById('grid-recentlyAdded');
GRID_CONTAINER = document.getElementById('grid-container');
PREVIEW_CONTAINER = document.getElementById('preview-container');
LIST_CONTAINER = document.getElementById('list-container');
TOGGLE_BUTTON = document.getElementById('header-toggle');
TOGGLE_GRID = document.getElementById('header-toggle-grid');
TOGGLE_PREVIEW = document.getElementById('header-toggle-preview');
TOGGLE_LIST = document.getElementById('header-toggle-list');
CONTENT_GRID_TOGGLE_RECENTLYADDED = document.getElementById('content-grid-toggle-recentlyAdded');
CONTENT_GRID_TOGGLE_ALL = document.getElementById('content-grid-toggle-all');

/*---- FUNCTIONS ----*/
//сбор инфы в объекты внутри массива
function getSourceArray() {
  'use strict';
  for (var i = 0; i < DATA_BASE.children.length; i++) {
    MAIN_ARRAY[i] = {
      album: DATA_BASE.children[i].dataset.album,
      artist: DATA_BASE.children[i].dataset.artist,
      year: DATA_BASE.children[i].dataset.year,
      img: DATA_BASE.children[i].dataset.img,
      url: DATA_BASE.children[i].dataset.url
    };
  }
}

//функция сортировки по полю
function byField(field) {
  return function(a, b) {
    var aa, bb;
    aa = a['album'].indexOf('‡') > -1 ? a['album'].slice(1) : a['album'];
    bb = b['album'].indexOf('‡') > -1 ? b['album'].slice(1) : b['album'];
    if (field === 'album') return aa.toLowerCase() + a['year'] + a['artist'].toLowerCase() > bb.toLowerCase() + b['year'] + a['artist'].toLowerCase() ? 1 : -1;
    if (field === 'artist') return a[field].toLowerCase() + a['year'] + aa.toLowerCase() > b[field].toLowerCase() + b['year'] + bb.toLowerCase() ? 1 : -1;
    if (field === 'year') return a[field] + aa.toLowerCase() + a['artist'].toLowerCase() > b[field] + bb.toLowerCase() + b['artist'].toLowerCase() ? 1 : -1;
  }
}

//создание больших превью
function createBlockGrid() {
  var grid_ul, grid_li, grid_div, grid_a, grid_span, grid_img, grid_p_album, grid_p_artist, grid_li_clone;
  grid_ul = document.createElement('ul');
  grid_ul.className = 'flex';
  grid_ul.id = 'grid_ul';
  grid_li = document.createElement('li');
  grid_li.className = 'flex-item';
  grid_div = document.createElement('div');
  grid_div.className = 'imageBox';
  grid_a = document.createElement('a');
  grid_span = document.createElement('span');
  grid_span.className = 'imageBox-border';
  grid_img = document.createElement('img');
  grid_img.className = 'imageBox-img';
  grid_p_album = document.createElement('p');
  grid_p_album.className = 'p album';
  grid_p_artist = document.createElement('p');
  grid_p_artist.className = 'p artist';

  grid_li.appendChild(grid_div);
  grid_div.appendChild(grid_a);
  grid_a.appendChild(grid_span);
  grid_a.appendChild(grid_img);
  grid_li.appendChild(grid_p_album);
  grid_li.appendChild(grid_p_artist);
  //добавление больших превью в контейнер
  for (var i = 0; i < MAIN_ARRAY.length; i++) {
    grid_li_clone = grid_li.cloneNode(true);
    grid_img.src = MAIN_ARRAY[i].img;
    grid_img.alt = MAIN_ARRAY[i].artist + ' — ' + MAIN_ARRAY[i].album;
    //если нет ссылки на альбом, ставить ссылку на мой сайт
    if (MAIN_ARRAY[i].url.length < 1) {
      grid_p_album.innerHTML = MAIN_ARRAY[i].album;
      grid_a.href = 'http://strash.ru';
      grid_p_album.title = MAIN_ARRAY[i].album;
    } else {
      grid_p_album.innerHTML = MAIN_ARRAY[i].album;
      grid_a.href = MAIN_ARRAY[i].url;
      grid_p_album.title = MAIN_ARRAY[i].album;
    }
    grid_p_artist.innerHTML = MAIN_ARRAY[i].artist + ' · ' + MAIN_ARRAY[i].year;
    grid_p_artist.title = MAIN_ARRAY[i].artist + ' · ' + MAIN_ARRAY[i].year;
    grid_a.target = '_blank';
    grid_ul.appendChild(grid_li_clone);
  }
  grid_ul.appendChild(grid_li);
  //удаление первого пустого блока из контейнера
  grid_ul.removeChild(grid_ul.children[0]);
  GRID_CONTAINER.innerHTML = '<H2 class="h2">All (' + MAIN_ARRAY.length + ')</h2>';
  //вставка контейнера с блоками на страницу
  GRID_CONTAINER.appendChild(grid_ul);
}

//создание последних добавленных больших превью
function createBlockGridRecentlyAdded() {
  var grid_ul, grid_li, grid_div, grid_a, grid_span, grid_img, grid_p_album, grid_p_artist, grid_li_clone, recentCounter;
  grid_ul = document.createElement('ul');
  grid_ul.className = 'flex';
  grid_ul.id = 'grid_ul';
  grid_li = document.createElement('li');
  grid_li.className = 'flex-item';
  grid_div = document.createElement('div');
  grid_div.className = 'imageBox';
  grid_a = document.createElement('a');
  grid_span = document.createElement('span');
  grid_span.className = 'imageBox-border';
  grid_img = document.createElement('img');
  grid_img.className = 'imageBox-img';
  grid_p_album = document.createElement('p');
  grid_p_album.className = 'p album';
  grid_p_artist = document.createElement('p');
  grid_p_artist.className = 'p artist';

  grid_li.appendChild(grid_div);
  grid_div.appendChild(grid_a);
  grid_a.appendChild(grid_span);
  grid_a.appendChild(grid_img);
  grid_li.appendChild(grid_p_album);
  grid_li.appendChild(grid_p_artist);
  //добавление больших превью в контейнер
  if (MAIN_ARRAY.length >= 20) recentCounter = 20;
  if (MAIN_ARRAY.length < 20) recentCounter = MAIN_ARRAY.length;
  for (var i = 0; i < recentCounter; i++) {
    grid_li_clone = grid_li.cloneNode(true);
    grid_img.src = MAIN_ARRAY[i].img;
    grid_img.alt = MAIN_ARRAY[i].artist + ' — ' + MAIN_ARRAY[i].album;
    //если нет ссылки на альбом, ставить ссылку на мой сайт
    if (MAIN_ARRAY[i].url.length < 1) {
      grid_p_album.innerHTML = MAIN_ARRAY[i].album;
      grid_a.href = 'http://strash.ru';
      grid_p_album.title = MAIN_ARRAY[i].album;
    } else {
      grid_p_album.innerHTML = MAIN_ARRAY[i].album;
      grid_a.href = MAIN_ARRAY[i].url;
      grid_p_album.title = MAIN_ARRAY[i].album;
    }
    grid_p_artist.innerHTML = MAIN_ARRAY[i].artist + ' · ' + MAIN_ARRAY[i].year;
    grid_p_artist.title = MAIN_ARRAY[i].artist + ' · ' + MAIN_ARRAY[i].year;
    grid_a.target = '_blank';
    grid_ul.appendChild(grid_li_clone);
  }
  grid_ul.appendChild(grid_li);
  //удаление первого пустого блока из контейнера
  grid_ul.removeChild(grid_ul.children[0]);
  GRID_RECENTLYADDED.innerHTML = '<H2 class="h2">Recently added</h2>';
  //вставка контейнера с блоками на страницу
  GRID_RECENTLYADDED.appendChild(grid_ul);
}

//создание маленьких превью
function createBlockPreview() {
  var preview_ul, preview_li, preview_img, preview_a, preview_li_clone;
  preview_ul = document.createElement('ul');
  preview_ul.className = 'flexPrev';
  preview_ul.id = 'preview_ul';
  preview_li = document.createElement('li');
  preview_li.className = 'flexPrev-item';
  preview_img = document.createElement('img');
  preview_img.className = 'imageBox-img';
  preview_a = document.createElement('a');

  preview_li.appendChild(preview_a);
  preview_a.appendChild(preview_img);
  //добавление маленьких превью в контейнер
  for (var i = 0; i < MAIN_ARRAY.length; i++) {
    //если нет ссылки на альбом, то ссылка на мой сайт
    if (MAIN_ARRAY[i].url.length < 1) {
      preview_a.href = 'http://strash.ru';
      preview_img.title = MAIN_ARRAY[i].artist + ' — ' + MAIN_ARRAY[i].album + ' (' + MAIN_ARRAY[i].year + ')';
      preview_img.alt = MAIN_ARRAY[i].artist + ' — ' + MAIN_ARRAY[i].album;
    } else {
      preview_a.href = MAIN_ARRAY[i].url;
      preview_img.title = MAIN_ARRAY[i].artist + ' — ' + MAIN_ARRAY[i].album + ' (' + MAIN_ARRAY[i].year + ')';
      preview_img.alt = MAIN_ARRAY[i].artist + ' — ' + MAIN_ARRAY[i].album;
    }
    preview_li_clone = preview_li.cloneNode(true);
    preview_img.src = MAIN_ARRAY[i].img;
    preview_a.target = '_blank';
    preview_ul.appendChild(preview_li_clone);
  }
  preview_ul.appendChild(preview_li);
  //вставка контейнера с блоками на страницу
  PREVIEW_CONTAINER.appendChild(preview_ul);
  //удаление первого пустого блока из контейнера
  preview_ul.removeChild(preview_ul.children[0]);
  resizePreview();
}

//создание списка
function createBlockList() {

}

//удаление сорцов со страницы
function removeSource() {
  DATA_BASE.parentNode.removeChild(DATA_BASE);
}

//скролл-прогресс
function progress() {
  var docHeight, progressBar, topScroll, windowHeight;
  docHeight = Math.max(
    document.body.scrollHeight,
    document.body.offsetHeight,
    document.body.clientHeight,
    document.documentElement.scrollHeight,
    document.documentElement.offsetHeight,
    document.documentElement.clientHeight
  );
  progressBar = document.getElementById('progressBar');
  topScroll = window.pageYOffset || document.body.scrollTop || document.documentElement.scrollTop;
  windowHeight = document.documentElement.clientHeight;
  progressBar.style.width = (topScroll * 100 / (docHeight - document.documentElement.clientHeight)) * document.documentElement.clientWidth / 100 + 'px';
}

//автоматический год
function getFooterYear() {
  var y = document.getElementById('year');
  var d = new Date();
  if (d.getFullYear() === 2015) {
    y.innerHTML = '&copy; <a href="http://strash.ru" target="_blank">Strash</a> | Music &mdash; 2015';
  } else {
    y.innerHTML = '&copy; <a href="http://strash.ru" target="_blank">Strash</a> | Music &mdash; 2015&ndash;' + d.getFullYear();
  }
}

//резайзим превьюшки до полного заполнения экрана по горизонтали
function resizePreview() {
  var k, j;
  for (k = 45; k > 0; k--) {
    if (document.documentElement.clientWidth / k >= 100 && document.documentElement.clientWidth / k <= 200) {
      for (j = 0; j < preview_ul.children.length; j++) {
        preview_ul.children[j].style.width = document.documentElement.clientWidth / k + 'px';
        preview_ul.children[j].style.height = document.documentElement.clientWidth / k + 'px';
      }
      break;
    }
  }
}

//растягиваем фон по высоте, если он меньше высоты экрана
function minHeight(a) {
  var h;
  h = parseFloat(getComputedStyle(a, '').height);
  a.style.height = h < document.documentElement.clientHeight ? document.documentElement.clientHeight - 45 + 'px' : a.style.height = '';
}

//header toggle
function toggleView() {
  TOGGLE_PREVIEW.style.opacity = '0';
  TOGGLE_LIST.style.opacity = '0';
  TOGGLE_BUTTON.onclick = function () {
    if (TOGGLE_COUNTER === 1) {
      TOGGLE_GRID.style.opacity = '0';
      TOGGLE_PREVIEW.style.opacity = '0';
      TOGGLE_LIST.style.opacity = '';
      MAIN_GRID.style.display = 'none';
      MAIN_PREVIEW.style.display = 'none';
      MAIN_LIST.style.display = '';
      document.body.style.backgroundColor = 'hsl(222, 30%, 50%)';
      progressBar.style.width = '';
      window.scrollTo(0, LIST_UL_SCROLL_POSITION);
      TOGGLE_COUNTER++;
      return;
    }
    if (TOGGLE_COUNTER === 2) {
      TOGGLE_GRID.style.opacity = '1';
      TOGGLE_PREVIEW.style.opacity = '0';
      TOGGLE_LIST.style.opacity = '0';
      MAIN_GRID.style.display = '';
      MAIN_PREVIEW.style.display = 'none';
      MAIN_LIST.style.display = 'none';
      document.body.style.backgroundColor = '';
      progressBar.style.width = '';
      window.scrollTo(0, GRID_UL_SCROLL_POSITION);
      TOGGLE_COUNTER = 0;
      return;
    }
    TOGGLE_GRID.style.opacity = '0';
    TOGGLE_PREVIEW.style.opacity = '1';
    TOGGLE_LIST.style.opacity = '0';
    MAIN_GRID.style.display = 'none';
    MAIN_PREVIEW.style.display = '';
    MAIN_LIST.style.display = 'none';
    document.body.style.backgroundColor = 'hsl(222, 11%, 18%)';
    progressBar.style.width = '';
    window.scrollTo(0, PREVIEW_UL_SCROLL_POSITION);
    TOGGLE_COUNTER++;
  };
  TOGGLE_BUTTON.onmouseover = function () {
    TOGGLE_GRID.style.fill = 'hsl(0,0%,100%)';
    TOGGLE_PREVIEW.style.fill = 'hsl(0,0%,100%)';
    TOGGLE_LIST.style.fill = 'hsl(0,0%,100%)';
  };
  TOGGLE_BUTTON.onmouseout = function () {
    TOGGLE_GRID.style.fill = '';
    TOGGLE_PREVIEW.style.fill = '';
    TOGGLE_LIST.style.fill = '';
  };
}

//content recetly/all toggle
function contentGridRecentlyToggle() {
  GRID_RECENTLYADDED.style.display = 'none';
  CONTENT_GRID_TOGGLE_RECENTLYADDED.onclick = function () {
    GRID_RECENTLYADDED.style.display = '';
    GRID_CONTAINER.style.display = 'none';
    CONTENT_GRID_TOGGLE_RECENTLYADDED.className = 'content-grid-toggle-item content-grid-toggle-item__active';
    CONTENT_GRID_TOGGLE_ALL.className = 'content-grid-toggle-item';
  };
  CONTENT_GRID_TOGGLE_ALL.onclick = function () {
    GRID_RECENTLYADDED.style.display = 'none';
    GRID_CONTAINER.style.display = '';
    CONTENT_GRID_TOGGLE_RECENTLYADDED.className = 'content-grid-toggle-item';
    CONTENT_GRID_TOGGLE_ALL.className = 'content-grid-toggle-item content-grid-toggle-item__active';
  };
}

//фильтрация
function filtrAllContent() {
  var filtr_album, filtr_artist, filtr_year;
  filtr_album = document.getElementById('filtr-album');
  filtr_artist = document.getElementById('filtr-artist');
  filtr_year = document.getElementById('filtr-year');
  filtr_artist.className = 'header-filtr-item header-filtr-item__active';
  filtr_album.className = 'header-filtr-item header-filtr-item__hover';
  filtr_year.className = 'header-filtr-item header-filtr-item__hover';

  filtr_album.onclick = function() {
    MAIN_ARRAY.sort(byField('album'));
    filtr_album.className = 'header-filtr-item header-filtr-item__active';
    filtr_artist.className = 'header-filtr-item header-filtr-item__hover';
    filtr_year.className = 'header-filtr-item header-filtr-item__hover';
    GRID_CONTAINER.innerHTML = '';
    PREVIEW_CONTAINER.innerHTML = '';
    createBlockGrid();
    createBlockPreview();
    gridBlocksHover();
  };
  filtr_artist.onclick = function() {
    MAIN_ARRAY.sort(byField('artist'));
    filtr_album.className = 'header-filtr-item header-filtr-item__hover';
    filtr_artist.className = 'header-filtr-item header-filtr-item__active';
    filtr_year.className = 'header-filtr-item header-filtr-item__hover';
    GRID_CONTAINER.innerHTML = '';
    PREVIEW_CONTAINER.innerHTML = '';
    createBlockGrid();
    createBlockPreview();
    gridBlocksHover();
  };
  filtr_year.onclick = function() {
    MAIN_ARRAY.sort(byField('year'));
    filtr_album.className = 'header-filtr-item header-filtr-item__hover';
    filtr_artist.className = 'header-filtr-item header-filtr-item__hover';
    filtr_year.className = 'header-filtr-item header-filtr-item__active';
    GRID_CONTAINER.innerHTML = '';
    PREVIEW_CONTAINER.innerHTML = '';
    createBlockGrid();
    createBlockPreview();
    gridBlocksHover();
  };
}

//ховер на блоки
function gridBlocksHover() {
  var jC, kC;
  for (jC = 0; jC < GRID_CONTAINER.children[1].children.length; jC++) {
    GRID_CONTAINER.children[1].children[jC].onmouseover = function () {
      this.style.backgroundColor = 'hsl(29,10%,93%)';
      this.children[0].children[0].children[0].style.boxShadow = 'inset 0 0 .5px 1px hsla(0, 0%, 0%, 0)';
    };
    GRID_CONTAINER.children[1].children[jC].onmouseout = function () {
      this.style.backgroundColor = '';
      this.children[1].style.color = '';
      this.children[2].style.color = '';
      this.children[0].children[0].children[0].style.boxShadow = '';
    };
  }
  for (kC = 0; kC < GRID_RECENTLYADDED.children[1].children.length; kC++) {
    GRID_RECENTLYADDED.children[1].children[kC].onmouseover = function () {
      this.style.backgroundColor = 'hsl(29,10%,93%)';
      this.children[0].children[0].children[0].style.boxShadow = 'inset 0 0 .5px 1px hsla(0, 0%, 0%, 0)';
    };
    GRID_RECENTLYADDED.children[1].children[kC].onmouseout = function () {
      this.style.backgroundColor = '';
      this.children[1].style.color = '';
      this.children[2].style.color = '';
      this.children[0].children[0].children[0].style.boxShadow = '';
    };
  }
}

/*---- BROWSER EVENTS ----*/
window.onload = function () {
  getSourceArray();
  createBlockGridRecentlyAdded();
  //сортировка по полям (album, artist, year, img, url)
  MAIN_ARRAY.sort(byField('artist'));
  //вызов функций
  createBlockGrid();
  createBlockPreview();
  removeSource();
  getFooterYear();
  toggleView();
  contentGridRecentlyToggle();
  gridBlocksHover();
  minHeight(MAIN_GRID);
  minHeight(MAIN_PREVIEW);
  minHeight(MAIN_LIST);
  filtrAllContent();
  //при загрузки страницы прятать превьюшки
  if (MAIN_PREVIEW !== null) {
    MAIN_PREVIEW.style.display = 'none';
    MAIN_LIST.style.display = 'none';
  }
};

window.onscroll = function () {
  progress();
  //при переключении вида возвращать на ту же позицию скрола
  if (TOGGLE_COUNTER === 0) {
    GRID_UL_SCROLL_POSITION = window.pageYOffset || document.body.scrollTop || document.documentElement.scrollTop;
  }
  if (TOGGLE_COUNTER === 1) {
    PREVIEW_UL_SCROLL_POSITION = window.pageYOffset || document.body.scrollTop || document.documentElement.scrollTop;
  }
  if (TOGGLE_COUNTER === 2) {
    LIST_UL_SCROLL_POSITION = window.pageYOffset || document.body.scrollTop || document.documentElement.scrollTop;
  }
  minHeight(MAIN_GRID);
  minHeight(MAIN_PREVIEW);
  minHeight(MAIN_LIST);
};

window.onresize = function () {
  progress();
  resizePreview();
  minHeight(MAIN_GRID);
  minHeight(MAIN_PREVIEW);
  minHeight(MAIN_LIST);
};
